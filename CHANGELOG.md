## 0.5.3

### Changed

- Ensure the body in agent bulk unlink endpoint converted to json


## 0.5.2

### Changed

- Add missing starting `/` to agent unlink and agent bulk unlink endpoints


## 0.5.1

### Added

- Add agent unlink functionality
- Add agent bulk unlink functionality


## 0.5

### Changed

Raise TenableError in `http_get_low` and `http_post_low` for various scenarios: 

- If the request fails and there are no more retries
- in case of `URI::InvalidURIError`
- if response code is not 200


## 0.4.4

### Changed

- Support passing in folder ID directly instead of folder name in `scan_quick_policy`
- Raise exception if folder name can not be found instead of failing silently 

## 0.4.3

### Changed

- Fix typo in target groups (thanks @neilramsay)

## 0.4.2

### Added

- Add target groups functionality (thanks @neilramsay)

## 0.4.1

### Changed

- Improve error handling from `scan_status` when status is missing in response

## 0.4.0

### Changed

- Make `scan_status` use https://developer.tenable.com/reference#scans-get-latest-status endpoint
- Remove `scan_latest_history_status` which is now unnecessary

## 0.3.9

### Added

- Add `asset_info` and `scans_plugin_output` endpoints

## 0.3.8

### Added

- Include the `response` attribute in TenableError when the server returns an error

## 0.3.7

### Changed

- Add missing slash to URL in `agent_group_list` endpoint


## 0.3.6

### Added

- Added `agent_group_list` endpoint
- Added `agent_group_delete` endpoint


## 0.3.5

### Changed

-  Allow `params` for `agent_list` endpoint


## 0.3.4

### Changed

-  Raise TenableError if `http_post_low` response code is not 200 (not `http_post`)


## 0.3.3

### Changed

-  Raise TenableError if `http_post` response code is not 200


## 0.3.2

### Changed

- `scan_delete` to return response

### Other

- Update API documentation links to developer.tenable.com


## 0.3.1

### Changed

- in `scan_quick_policy` raise `TenableError` if cannot get folders, policies or if a policy with that name does not exist

## 0.3.0

### Added

- Added `agent_list` endpoint
- Added `agent_group_create` endpoint
- Added `agent_group_add_agent` endpoint

### Changed

- Change the signature of the `scan_quick_policy` function to accept a
  hash instead of a string of targets

## 0.2.10

### Added

- Added some Tenable.io container security api endpoints (thanks @eightzerobits)

### Changed

- in `scan_status` and `scan_latest_history_status` raise `TenableError`

## 0.2.9

### Changed

- in `report_download_quick` raise `TenableError` if export status is `nil`, `""` or `"error"`

### Other

- Clean up README, add CHANGELOG and CONTRIBUTING
- Remove references to Nessus Professional, API has been deactivated in latest version
- Clean up code

