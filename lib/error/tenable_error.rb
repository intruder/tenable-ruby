module TenableRuby
  module Error
    class TenableError < StandardError
      attr_reader :response

      def initialize(message=nil, response: nil)
        @response = response
        super(message)
      end
    end
  end
end